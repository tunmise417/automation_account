variable "runbook_name" {
  type = string
}

variable "automation_account_name" {
  type = string
}

variable "location" {
  type    = string
  default = ""
}

variable "resource_group_name" {
  type = string
}

variable "log_verbose" {
  type = bool
  default = true
}

variable "log_progress" {
  type = bool
  default = true
}

variable "description" {
  type = string
  default = ""
}

variable "runbook_type" {
    type = string
    description = "This defines type of your runbook. Supported values are Graph, GraphPowerShell, GraphPowerShellWorkflow, PowerShellWorkflow, PowerShell, Python3, Python2 or Script"
    default = "PowerShell" 
}

variable "publish_content_link" {
  type = string
  default = ""
}

variable "content_file_path" {
    type = string
    default = ""
}