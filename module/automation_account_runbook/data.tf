
data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}


data "azurerm_automation_account" "account" {
    name = var.automation_account_name
    resource_group_name = var.resource_group_name
}

data "local_file" "file" {
  count = var.content_file_path != "" ? 1 : 0
  filename = var.content_file_path
}
