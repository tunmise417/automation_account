
resource "azurerm_automation_runbook" "runbook" {
  name                    = var.runbook_name
  location                = var.location == "" ? data.azurerm_resource_group.rg.location : var.location
  resource_group_name     = data.azurerm_resource_group.rg.name
  automation_account_name = data.azurerm_automation_account.account.name
  log_verbose             = var.log_verbose
  log_progress            = var.log_verbose
  description             = var.description
  runbook_type            = var.runbook_type

  content = var.content_file_path != "" && var.publish_content_link == "" ? data.local_file.file[0].content : null

  dynamic publish_content_link {
    for_each = var.publish_content_link != "" && var.content_file_path == ""? [1] : [0]

    content {
      uri = var.publish_content_link
  }
}
}