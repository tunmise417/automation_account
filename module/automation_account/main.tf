
# Create Automation Account
resource "azurerm_automation_account" "account" {
  name                = var.automation_account_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.location == "" ? data.azurerm_resource_group.rg.location : var.location
  sku_name            = try(var.sku_name, "Basic")
  tags                = var.tags
  public_network_access_enabled = var.public_network_access_enabled
  identity {
    type = "SystemAssigned"
  }
}


# Create diagnostics settings for Automation Account
resource "azurerm_monitor_diagnostic_setting" "account" {
  depends_on = [
    azurerm_automation_account.account
  ]

  name                           = "azure-diagnostic-setting-0"
  target_resource_id             = azurerm_automation_account.account.id
  log_analytics_workspace_id     = var.diagnostics.type == "log_analytics" ? data.azurerm_log_analytics_workspace.diagnostics[0].id : null
  storage_account_id             = var.diagnostics.type == "storage" ? data.azurerm_storage_account.diagnostics[0].id : null
  log_analytics_destination_type = "AzureDiagnostics"

  dynamic "log" {
    for_each = var.diagnostics.logs != [] ? var.diagnostics.logs : []

    content {
      category = log.value
      enabled  = true

      #retention policy applies when output type is storage account
      retention_policy {
        enabled = var.diagnostics.type == "storage" ? true : false
        days    = var.diagnostics.type == "storage" ? var.diagnostics.retention : null
      }
    }
  }

  dynamic "metric" {
    for_each = var.diagnostics.metric != [] ? var.diagnostics.metric : []

    content {
      category = metric.value
      enabled  = true

      #retention policy applies when output type is storage account
      retention_policy {
        enabled = var.diagnostics.type == "storage" ? true : false
        days    = var.diagnostics.type == "storage" ? var.diagnostics.retention : null
      }
    }
  }
}


# Create a private endpoint for automation account

resource "azurerm_private_endpoint" "account" {

  depends_on = [
    azurerm_automation_account.account
  ]
  count               = var.enable_pvt_endpoint == true ? 1 : 0
  name                = join("", [azurerm_automation_account.account.name, "Webhook"])
  location            = var.pvt_endpoint_location == "" ? data.azurerm_resource_group.pvt_endpoint_rg[0].location : var.pvt_endpoint_location
  resource_group_name = data.azurerm_resource_group.pvt_endpoint_rg[0].name
  subnet_id           = data.azurerm_subnet.endpoint_subnet[0].id
  tags                = var.tags

  private_service_connection {
    name                           = join("", [azurerm_automation_account.account.name, "-pvt-endpoint-conn"])
    private_connection_resource_id = azurerm_automation_account.account.id
    subresource_names              = ["Webhook"]
    is_manual_connection           = false
  }
}

resource "azurerm_private_endpoint" "account2" {

  depends_on = [
    azurerm_automation_account.account
  ]
  # count               = var.enable_pvt_endpoint == true ? 1 : 0
  name                = join("", [azurerm_automation_account.account.name, "DSCAndHybridWorker"])
  location            = var.pvt_endpoint_location == "" ? data.azurerm_resource_group.pvt_endpoint_rg[0].location : var.pvt_endpoint_location
  resource_group_name = data.azurerm_resource_group.pvt_endpoint_rg[0].name
  subnet_id           = data.azurerm_subnet.endpoint_subnet[0].id
  tags                = var.tags

  private_service_connection {
    name                           = join("", [azurerm_automation_account.account.name, "-pvt-endpoint-conn"])
    private_connection_resource_id = azurerm_automation_account.account.id
    subresource_names              = ["DSCAndHybridWorker"]
    is_manual_connection           = false
  }
}