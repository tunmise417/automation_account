data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

data "azurerm_resource_group" "diagnostics_rg" {
  name     = var.diagnostics_resource_group_name
  provider = azurerm.diagnostics
}

data "azurerm_resource_group" "pvt_endpoint_rg" {
  count = var.enable_pvt_endpoint == true ? 1 : 0
  name = var.pvt_endpoint_resource_group
}

data "azurerm_log_analytics_workspace" "diagnostics" {
  count               = var.diagnostics_log_analytics_workspace != "" ? 1 : 0
  name                = var.diagnostics_log_analytics_workspace
  resource_group_name = data.azurerm_resource_group.diagnostics_rg.name
  provider            = azurerm.diagnostics
}

data "azurerm_storage_account" "diagnostics" {
  count = var.diagnostics_storage_account != "" ? 1 : 0

  provider            = azurerm.diagnostics
  name                = var.diagnostics_storage_account
  resource_group_name = data.azurerm_resource_group.diagnostics_rg.name
}

data "azurerm_subnet" "endpoint_subnet" {
  count                = var.enable_pvt_endpoint == true ? 1 : 0
  name                 = var.pvt_endpoint_subnet.name
  resource_group_name  = var.pvt_endpoint_subnet.resource_group_name
  virtual_network_name = var.pvt_endpoint_subnet.virtual_network_name
}