output "id" {
  value = azurerm_automation_account.account.id
}

output "name" {
  value = azurerm_automation_account.account.name
}

output "pvt_endpoint_id" {
  value = azurerm_private_endpoint.account[0].id
}