variable "automation_account_name" {
  type = string
}

variable "location" {
  type    = string
  default = ""
}

variable "resource_group_name" {
  type = string
}

variable "sku_name" {
  type    = string
  default = "Basic"
}

variable "tags" {
  type    = map(any)
  default = {}
}

variable "public_network_access_enabled" {
  type = bool
  default = true
}

# Diagnostics Variables

variable "diagnostics_log_analytics_workspace" {
  type    = string
  default = ""
}

variable "diagnostics_storage_account" {
  type    = string
  default = ""
}

variable "diagnostics_resource_group_name" {
  type = string
}

variable "diagnostics" {
  type = object({
    logs      = list(string)
    metric    = list(string)
    retention = number
    type      = string
  })
  default = null
}

# Private Endpoint Variables

variable "enable_pvt_endpoint" {
  type    = bool
  default = false
}

variable "pvt_endpoint_resource_group" {
  type    = string
  default = ""
}

variable "pvt_endpoint_location" {
  type    = string
  default = ""
}

variable "pvt_endpoint_subnet" {
  description = "A map of variables to use for the Endpoint Subnet."
  type = object({
    name                 = string
    resource_group_name  = string
    virtual_network_name = string
  })
  default = null
}

variable "subresource_names" {
  description = "subresource name"
}