# provider "azurerm" {
#   features {}
# }

# provider "azurerm" {
#   features {

#   }
#   subscription_id = "1d0b75e4-49ca-42fa-8857-b473a49dc028"
#   alias           = "diagnostics"
# }

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      configuration_aliases = [ azurerm, azurerm.diagnostics ]
    }
  }
}
