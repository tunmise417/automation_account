Write-Host "Hello World"
param(
    [Parameter(
      HelpMessage="Server to connect to"
    )] [string] $server = "sql-mednet3-dev-centralus.database.windows.net",
    [Parameter(      
      HelpMessage="Database to connect to"
    )] [string] $database = "mednet3dev",
    [Parameter( 
	    Mandatory=$true,	     
      HelpMessage="SQL Admin credential"
    )] [string] $username,
    [Parameter( 
	    Mandatory=$true,	     
      HelpMessage="SQL Admin credential"
    )] [string] $password)

$user = New-Object -TypeName "System.Management.Automation.PSCredential" `
    -ArgumentList "$username",("$password" | ConvertTo-SecureString -AsPlainText -Force)

$SQLQuery = @'

Invoke-Sqlcmd -ServerInstance $server -Credential $user -Database $database -Query $SQLQuery -QueryTimeout 65535 -ConnectionTimeout 60 -Verbose