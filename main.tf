provider "azurerm" {
  features {}
}

provider "azurerm" {
  features {}
  subscription_id = "1d0b75e4-49ca-42fa-8857-b473a49dc028"
  alias           = "diagnostics"
}

module "automation-account" {
    source = "./module/automation_account"
    providers = {
      azurerm = azurerm
      azurerm.diagnostics = azurerm.diagnostics
     }
    automation_account_name = "test-auto-account"
    resource_group_name = "Tumi-RG"
    public_network_access_enabled = false
    diagnostics_log_analytics_workspace = "" # Pass log analytics name if want to onboard logs to log analytics workspace
    diagnostics_resource_group_name = "Tumi-RG"
    diagnostics_storage_account = "teststr007"
    diagnostics = {
                    type = "storage" # Change to 'log_analytics' if want to onboard logs to log analytics workspace
                    logs = [ "JobLogs", "JobStreams", "DscNodeStatus", "AuditEvent" ]
                    metric = [ "AllMetrics" ]
                    retention = 7
                    }
    enable_pvt_endpoint = true # Set this value to true if want to create pvt endpoint
    subresource_names = "Webhook"
    pvt_endpoint_resource_group = "Tumi-RG"
    pvt_endpoint_subnet = {
                            name = "default"
                            resource_group_name = "Tumi-RG"
                            virtual_network_name = "testvnet"
                            }
}

# Runbook creation using file content embedded to module path like runbook.ps1
module "runbook_01" {
  source = "./module/automation_account_runbook"
  runbook_name = "test_runbook_01"
  description = "This is a test runbook"
  resource_group_name = "Tumi-RG"
  automation_account_name = module.automation-account.name
  log_verbose   = true
  log_progress = true
  runbook_type  = "PowerShell"
  content_file_path = "${path.module}/CommandExecute.ps1"
  depends_on = [
    module.automation-account
  ]
}

# Runbook creation using published link to file (stored in a storage account)
module "runbook_02" {
  source = "./module/automation_account_runbook"
  runbook_name = "test_runbook_02"
  description = "This is a test runbook"
  resource_group_name = "Tumi-RG"
  automation_account_name = module.automation-account.name
  log_verbose   = true
  log_progress = true
  runbook_type  = "PowerShell"
  content_file_path = "${path.module}/CommandLog.ps1"
  depends_on = [
    module.automation-account
  ]
}