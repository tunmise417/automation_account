output "automation_account_id" {
  value = module.automation-account.id
}

output "automation_account_name" {
  value = module.automation-account.name
}

output "pvt_endpoint_id" {
  value = module.automation-account.pvt_endpoint_id
}

output "runbook_01" {
  value = module.runbook_01.runbook_name
}

output "runbook_02" {
  value = module.runbook_02.runbook_name
}